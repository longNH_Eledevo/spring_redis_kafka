package vn.longnh.sale_service.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthExternalResponse {
    private String requestId;
    private String userId;
    private String userEmail;
    private String role;
    private boolean isAuthenticated;
}

package vn.longnh.sale_service.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthExternalRequest {
    private String requestId;
    private String token;
}

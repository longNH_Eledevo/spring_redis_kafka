package vn.longnh.sale_service.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import vn.longnh.sale_service.dto.request.AuthExternalRequest;
import vn.longnh.sale_service.dto.response.AuthExternalResponse;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

@Service
@RequiredArgsConstructor
public class SaleService {

    @Value("${application.kafka.topic.auth-request}")
    private String authRequestTopic;

    @Value("${application.kafka.topic.storage}")
    private String storageTopic;

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final Map<String, BlockingQueue<AuthExternalResponse>> authResponseQueues = new ConcurrentHashMap<>();

    private final ObjectMapper objectMapper;

    public boolean authenticateRequest(String authHeader) throws JsonProcessingException {
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return false;
        }
        String token = authHeader.substring(7);
        String requestId = UUID.randomUUID().toString();
        AuthExternalRequest request = new AuthExternalRequest(requestId, token);
        String requestJson = objectMapper.writeValueAsString(request);
        kafkaTemplate.send(authRequestTopic, requestJson);
        BlockingQueue<AuthExternalResponse> authResponseQueue = new LinkedBlockingQueue<>();
        authResponseQueues.put(requestId, authResponseQueue);
        try {
            AuthExternalResponse authResponse = authResponseQueue.take();
            return authResponse.isAuthenticated();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    @KafkaListener(topics = "${application.kafka.topic.auth-response}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleAuthResponse(String responseJson) throws JsonProcessingException {
        AuthExternalResponse response = objectMapper.readValue(responseJson, AuthExternalResponse.class);
        String requestId = response.getRequestId();
        BlockingQueue<AuthExternalResponse> authResponseQueue = authResponseQueues.get(requestId);
        if (authResponseQueue != null) {
            authResponseQueue.offer(response);
            authResponseQueues.remove(requestId);
        }
    }

    public String helloSaleService() {
        String msg = " _                             ______   _     _   \n" +
                "| |                           |  ___ \\ | |   | |  \n" +
                "| |        ___   ____    ____ | |   | || |__ | |  \n" +
                "| |       / _ \\ |  _ \\  / _  || |   | ||  __)| |  \n" +
                "| |_____ | |_| || | | |( ( | || |   | || |   | |  \n" +
                "|_______) \\___/ |_| |_| \\_|| ||_|   |_||_|   |_|  \n" +
                "                       (_____|                    \n";
        kafkaTemplate.send(storageTopic, msg);
        return "Hello Sale Service!!!";
    }

}

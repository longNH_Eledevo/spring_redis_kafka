package vn.longnh.sale_service.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration

public class KafkaTopicConfig {

    @Value("${application.kafka.topic.storage}")
    private String storageTopic;

    @Bean
    public NewTopic storageTopic() {
        return TopicBuilder
                .name(storageTopic)
                .build();
    }

}

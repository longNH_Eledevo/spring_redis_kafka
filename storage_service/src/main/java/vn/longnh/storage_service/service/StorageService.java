package vn.longnh.storage_service.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class StorageService {
    @KafkaListener(topics = "${application.kafka.topic.storage}")
    private void listener(String msg) {
        System.out.println(msg);
    }
}

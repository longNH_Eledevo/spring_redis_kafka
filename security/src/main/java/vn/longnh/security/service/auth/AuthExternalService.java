package vn.longnh.security.service.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import vn.longnh.security.config.security.JwtService;
import vn.longnh.security.dto.request.AuthExternalRequest;
import vn.longnh.security.dto.response.AuthExternalResponse;
import vn.longnh.security.entity.User;
import vn.longnh.security.repository.UserRepository;
import vn.longnh.security.repository.redis.BaseRedisRepository;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthExternalService {
    @Value("${application.kafka.topic.auth-request}")
    private String authRequestTopic;

    @Value("${application.kafka.topic.auth-response}")
    private String authResponseTopic;

    @Value("${spring.kafka.consumer.group-id}")
    private String group;

    private final KafkaTemplate<String, String> authExternalResponseKafkaTemplate;

    private final JwtService jwtService;

    private final UserRepository userRepository;

    private final BaseRedisRepository redisRepository;

    private final ObjectMapper objectMapper;

    @KafkaListener(topics = "${application.kafka.topic.auth-request}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleAuthRequest(String requestJson) throws JsonProcessingException {
        AuthExternalRequest request = objectMapper.readValue(requestJson, AuthExternalRequest.class);
        AuthExternalResponse response = new AuthExternalResponse();
        response.setRequestId(request.getRequestId());
        response.setAuthenticated(false);

        String token = request.getToken();
        String userEmail;

        try {
            userEmail = jwtService.extractUsername(token);
            boolean isTokenExist = Objects.equals(redisRepository.get(userEmail), token);
            if (!isTokenExist) {
                authExternalResponseKafkaTemplate.send(authResponseTopic, objectMapper.writeValueAsString(response));
                return;
            }
        } catch (Exception e) {
            authExternalResponseKafkaTemplate.send(authResponseTopic, objectMapper.writeValueAsString(response));
            return;
        }

        Optional<User> userOptional = userRepository.findByEmail(userEmail);
        if (userOptional.isEmpty()) {
            authExternalResponseKafkaTemplate.send(authResponseTopic, objectMapper.writeValueAsString(response));
            return;
        }

        User user = userOptional.get();
        response.setAuthenticated(true);
        response.setUserId(user.getId());
        response.setUserEmail(user.getEmail());
        response.setRole(user.getRole());
        authExternalResponseKafkaTemplate.send(authResponseTopic, objectMapper.writeValueAsString(response));
    }

}

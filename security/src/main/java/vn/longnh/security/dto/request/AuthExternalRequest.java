package vn.longnh.security.dto.request;

import lombok.Data;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthExternalRequest {
    private String requestId;
    private String token;
}

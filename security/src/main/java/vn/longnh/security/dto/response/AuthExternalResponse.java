package vn.longnh.security.dto.response;

import lombok.Data;
import vn.longnh.security.constant.Role;

@Data
public class AuthExternalResponse {
    private String requestId;
    private String userId;
    private String userEmail;
    private Role role;
    private boolean isAuthenticated;
}

package vn.longnh.security.config.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration

public class KafkaTopicConfig {

    @Value("${application.kafka.topic.auth-request}")
    private String authRequestTopic;

    @Value("${application.kafka.topic.auth-response}")
    private String authResponseTopic;

    @Bean
    public NewTopic authRequestTopic() {
        return TopicBuilder
                .name(authRequestTopic)
                .build();
    }

    @Bean
    public NewTopic authResponseTopic() {
        return TopicBuilder
                .name(authResponseTopic)
                .build();
    }
}
